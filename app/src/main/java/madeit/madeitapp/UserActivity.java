package madeit.madeitapp;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by konni on 22.07.15.
 */
public class UserActivity extends MadeItActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        String info = "no user";
        if(MainActivity.user != null) {
            info = MainActivity.user.toString();
        }
        TextView userInfoField = (TextView)findViewById(R.id.userInfo);
        userInfoField.setText(info);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}