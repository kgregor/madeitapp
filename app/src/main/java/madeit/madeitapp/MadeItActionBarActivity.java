package madeit.madeitapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by konni on 22.07.15.
 */
public class MadeItActionBarActivity extends AppCompatActivity {

    protected ActionBar actionBar;
    protected SearchView searchView;
    protected MenuItem searchItem;
    protected MenuItem accountItem;
    protected MenuItem watchlistItem;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        System.out.println("set menu!!");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        final Activity currentActivity = this;

        accountItem = menu.findItem(R.id.action_account);
        watchlistItem = menu.findItem(R.id.action_watchlist);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent swipeIntent = new Intent(getApplicationContext(), ChallengeActivity.class);
                Bundle b = new Bundle();
                b.putString("searchQuery", query);

                if((currentActivity instanceof ChallengeActivity) && ((ChallengeActivity)currentActivity).isInSearchMode()){
                    ((ChallengeActivity)currentActivity).newSearch(query);
                } else {
                    swipeIntent.putExtras(b);
                    startActivity(swipeIntent);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }

        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        System.out.println("SELECTED " + id);
        if (id == R.id.action_account) {
            System.out.println("Show Account Info");
            Intent userIntent = new Intent(getApplicationContext(), UserActivity.class);
            Bundle b = new Bundle();
            userIntent.putExtras(b);
            startActivity(userIntent);
            return true;
        }
        if (id == R.id.action_watchlist) {
            System.out.println("Show Watchlist");
            Intent watchListIntent = new Intent(getApplicationContext(), ChallengeListActivity.class);
            Bundle b = new Bundle();
            watchListIntent.putExtras(b);
            startActivity(watchListIntent);
            return true;
        }

        System.out.println("check for home: " + id + " and " + android.R.id.home);

        if (id == android.R.id.home) {
            System.out.println("----------- HOME BUTTON -----------");
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideAllIcons(){
        searchView.setVisibility(View.GONE);
        searchItem.setVisible(false);
        watchlistItem.setVisible(false);
        accountItem.setVisible(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

}