package madeit.madeitapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by konni on 22.07.15.
 */
public class User {

    private String name;
    private String id;
    private List<Integer> challengeList;
    private HashSet<Integer> challengeListHelper;

    public User(String receivedUserInfo){
        System.out.println("USERINFO: " + receivedUserInfo);
        JSONObject userInfo = null;
        User user = null;
        try {
            userInfo = new JSONObject(receivedUserInfo);
            String userid = userInfo.getString("_id");
            String name = userInfo.getString("name");

            JSONArray challengelistJSON = userInfo.getJSONArray("challengelist");
            List<Integer> challengeList = new LinkedList<Integer>();
            if (challengelistJSON != null) {
                for (int i=0;i<challengelistJSON.length();i++){
                    challengeList.add(challengelistJSON.getInt(i));
                }
            }
            this.name = name;
            this.id = userid;
            this.challengeList = challengeList;
            setUpChallengeListHelper();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setUpChallengeListHelper() {
        this.challengeListHelper = new HashSet<Integer>();
        for(Integer filmid : challengeList){
            challengeListHelper.add(filmid);
        }
    }

    public User(String id, String name, int nRatings, List<Integer> challengeList){
        this.id = id;
        this.name = name;
        this.challengeList = challengeList;
        setUpChallengeListHelper();
    }

    public String getID() { return id; }

    public String getChallengeList() {
        StringBuilder strb = new StringBuilder();
        return challengeList.toString();
    }

    public String getName(){
        return this.name;
    }

    public boolean hasInChallengeList(int id){
        return challengeListHelper.contains(id);
    }

    public void removeFromChallengelist(int id) {
        challengeListHelper.remove(id);
        challengeList.remove(new Integer(id));
    }

    public void addToChallengelist(int id) {
        if(!challengeListHelper.contains(id)) {
            challengeListHelper.add(id);
            challengeList.add(id);
        }
    }
}