package madeit.madeitapp;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by konni on 22.07.15.
 */
public class ChallengeListActivity extends AppCompatActivity{

    private LinkedList<JSONObject> challengeList;

    private String server_url = "fjannis.volans.uberspace.de";
    private String server_port = "61808";
    private LinearLayout challengeListLinearLayout;
    private TextView challengeListLoadingField;
    private LinkedList<Bitmap> imageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challengelist);

        challengeListLoadingField = (TextView)findViewById(R.id.challengeListLoadingText);
        challengeListLinearLayout =  (LinearLayout)findViewById(R.id.challengeListLinearLayout);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume(){
        super.onResume();
        challengeListLinearLayout.removeAllViews();
        imageList = new LinkedList<Bitmap>();
        String info = "loading...";
        challengeListLoadingField.setText(info);
        if(MainActivity.user != null) {
            info = MainActivity.user.getChallengeList();
        }
        new FilmListGetterFromServer().execute(info);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class FilmListGetterFromServer extends AsyncTask<String, String, List<JSONObject>> {

        protected List<JSONObject> doInBackground(String... args) {
            System.out.println("#######  FILMLIST GETTER DO IN BACKGROUND!! " + args[0]);

            String challengeListIds = args[0].replace(" ", "");


            System.out.println("##### Challenge LIST: " + challengeListIds);

            challengeList = new LinkedList<JSONObject>();


            List<JSONObject> challengeListLocal;
            URL url = null;

            try {
                //TODO not yet implemented in server
                url = new URL("http://" + server_url + ":" + server_port + "/getchallengesbyids?ids=" + challengeListIds);
                System.out.println("connect to: " + url.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = null;
            try {
                System.out.println("get connection");
                urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println("################ 1");
                InputStreamReader isr = new InputStreamReader(urlConnection.getInputStream());
                System.out.println("############## 2");
                BufferedReader reader = new BufferedReader(isr);
                System.out.println("vor der while schleife");
                String line = reader.readLine();
                System.out.println("LINE: " + line);

                challengeListLocal = workAroundHandleJSON(line);


            } catch (IOException e) {
                System.out.println("Exception when loading challenge info");
                JSONObject errorJSON = null;
                try {
                    errorJSON = new JSONObject("ERROR");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                challengeListLocal = new LinkedList<JSONObject>();
                challengeListLocal.add(errorJSON);
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }

            return challengeListLocal;

        }

        private LinkedList<JSONObject> workAroundHandleJSON(String jsonString){

            LinkedList<JSONObject> challengeInfoList = new LinkedList<JSONObject>();

            try {
                JSONArray jArray  = new JSONArray(jsonString);
                System.out.println("JSON ARRAY LENGTH: " + jArray.length());
                for(int i=0; i<jArray.length(); i++) {
                    JSONObject entry = (JSONObject) jArray.get(i);
                    challengeInfoList.add(entry);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return challengeInfoList;

        }



        protected void onPostExecute(List<JSONObject> list) {

            challengeListLoadingField.setVisibility(View.GONE);
            if(list != null && !list.isEmpty()) {
                for(int i=0; i<list.size(); i++){
                    final JSONObject challenge = list.get(i);
                    try {

                        LinearLayout challengeInfo = new LinearLayout(ChallengeListActivity.this);
                        challengeInfo.setOrientation(LinearLayout.HORIZONTAL);
                        challengeInfo.setPadding(10, 10, 10, 10);

                        LinearLayout infoField = getInfoField(challenge);

                        challengeInfo.addView(infoField);

                        challengeListLinearLayout.addView(challengeInfo);

                        if(i < list.size() - 1){
                            TextView divider = new TextView(ChallengeListActivity.this);
                            divider.setWidth(10);
                            divider.setHeight(2);
                            divider.setBackgroundColor(getResources().getColor(R.color.divider_color));
                            divider.setAlpha(0.3f);
                            challengeListLinearLayout.addView(divider);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                challengeListLoadingField.setText("No Challengelist available...");
                challengeListLoadingField.setVisibility(View.VISIBLE);
            }

        }

        private LinearLayout getInfoField(JSONObject challenge) throws JSONException {
            LinearLayout infoField = new LinearLayout(ChallengeListActivity.this);
            infoField.setOrientation(LinearLayout.VERTICAL);

            TextView titleText = new TextView(ChallengeListActivity.this);
            titleText.setText(challenge.getString("Title"));
            titleText.setWidth(290);
            titleText.setTypeface(null, Typeface.BOLD);
            titleText.setEllipsize(TextUtils.TruncateAt.END);

            TextView infoText = new TextView(ChallengeListActivity.this);
            infoText.setWidth(290);
            infoText.setEllipsize(TextUtils.TruncateAt.END);
            infoText.setText(challenge.getString("Year") + " \u00B7 " + challenge.getString("Runtime"));

            infoField.addView(titleText);
            infoField.addView(infoText);
            return infoField;
        }



    }



}
