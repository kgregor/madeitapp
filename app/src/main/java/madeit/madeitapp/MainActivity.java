package madeit.madeitapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends MadeItActionBarActivity {

    private Button goToChallengesActivityButton;
    private Button loginButton;
    private EditText userNameTextField;
    private static final boolean LOCAL_RUN = false;
    public static String server_url;
    public static String server_port;
    private TextView loginText;
    public static User user = null;
    private Button registerButton;
    private static int displayWidth;
    private static int displayHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        actionBar = getSupportActionBar();

        //TODO always shows menu button, but this is a hack
        try {
            ViewConfiguration config = ViewConfiguration.get(MainActivity.this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //hides keyboard after login
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayWidth = size.x;
        displayHeight = size.y;

        userNameTextField = (EditText)findViewById(R.id.userNameField);
        loginText = (TextView)findViewById(R.id.loginText);

        goToChallengesActivityButton = (Button)findViewById(R.id.goToChallengesButton);
        goToChallengesActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent swipeIntent = new Intent(getApplicationContext(), ChallengeActivity.class);
                startActivity(swipeIntent);
            }
        });
        goToChallengesActivityButton.setVisibility(View.GONE);

        if(LOCAL_RUN){
            server_url = getResources().getString(R.string.local_server_url);
            goToChallengesActivityButton.setVisibility(View.VISIBLE);
            user = new User("bla", "bla" ,2, null);
        } else {
            server_url = getResources().getString(R.string.remote_server_url);
        }
        server_port = getResources().getString(R.string.server_port);

        registerButton = (Button)findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        loginButton = (Button)findViewById(R.id.loginButton);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(user == null || user.getID() == null) {
            actionBar.hide();
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new LoginManager().execute(userNameTextField.getText().toString());
                    InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            });
        } else {
            actionBar.show();
            loginButton.setVisibility(View.GONE);
            registerButton.setVisibility(View.GONE);
            userNameTextField.setVisibility(View.GONE);
            goToChallengesActivityButton.setVisibility(View.VISIBLE);
            goToChallengesActivityButton.setText("Get Recommendations!");
            loginText.setText("Hey " + user.getName() + "!");
        }
    }

    private class LoginManager extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            System.out.println("logging in");

            String username = args[0];
            String localUserInfo="-1";

            URL url = null;
            try {
                String password = "pw";

                System.out.println("log in as " + username + " and " + password);

                url = new URL("http://" + server_url + ":" + server_port + "/login?email=" + username + "&password=" + password);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = null;
            try {
                System.out.println("get connection");
                urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println("MongoDB URL called");
                String line = "";
                InputStreamReader isr = new InputStreamReader(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                localUserInfo = reader.readLine();

            } catch (IOException e) {
                System.out.println("Exception when submitting film ratings");
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }

            System.out.println("DONE");
            return localUserInfo;
        }

        @Override
        protected void onPostExecute(String receivedUserInfo){
            if(receivedUserInfo != null && !receivedUserInfo.equals("null")){
                MainActivity.user = new User(receivedUserInfo);
                onResume();
            } else {
                Toast.makeText(MainActivity.this, "Login unsuccessful!", Toast.LENGTH_SHORT).show();
            }
        }

    }


}
