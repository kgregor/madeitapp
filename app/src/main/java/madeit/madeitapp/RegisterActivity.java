package madeit.madeitapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created by konni on 22.07.15.
 */
public class RegisterActivity extends MadeItActionBarActivity {

    private Button registerButton;
    private Spinner spinYear;
    private Spinner spinGender;
    private Spinner spinNation;
    private EditText emailField;
    private String[] isoCountryCodes;
    private TextView togglePrivacyButton;
    private EditText nameField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        actionBar = getSupportActionBar();

        emailField = (EditText) findViewById(R.id.emailField);
        nameField = (EditText) findViewById(R.id.nameField);
        spinNation = (Spinner) findViewById(R.id.nationSpinner);
        spinGender = (Spinner) findViewById(R.id.genderSpinner);
        spinYear = (Spinner) findViewById(R.id.yearSpinner);
        setYearSpinner();
        setGenderSpinner();
        setNationalitySpinner();

        togglePrivacyButton = (TextView) findViewById(R.id.toggle_privacy_button);
        togglePrivacyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder privacyInfoAlert = new AlertDialog.Builder(new ContextThemeWrapper(RegisterActivity.this, R.style.AppTheme));
                final AlertDialog alertd = privacyInfoAlert.create();
                privacyInfoAlert.setTitle(R.string.privacy_headline);
                privacyInfoAlert.setMessage(R.string.privacy_text);
                privacyInfoAlert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                        alertd.dismiss();
                    }
                });
                privacyInfoAlert.show();
            }
        });

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String[] params = new String[6];
                params[0] = emailField.getText().toString();
                params[1] = nameField.getText().toString();
                params[2] = "pw";
                params[3] = spinYear.getSelectedItem().toString();
                if (spinNation.getSelectedItemPosition() < isoCountryCodes.length) {
                    params[4] = isoCountryCodes[spinNation.getSelectedItemPosition()];
                } else {
                    params[4] = "0";
                }
                switch (spinGender.getSelectedItem().toString()) {
                    case "Female":
                        params[5] = "F";
                        break;
                    case "Male":
                        params[5] = "M";
                        break;
                    case "Other":
                        params[5] = "O";
                        break;
                    case "n/a":
                        params[5] = "-";
                        break;
                }


                if (!allInputsProvided(params)) {
                    Toast.makeText(RegisterActivity.this, "Please provide all Information!", Toast.LENGTH_SHORT).show();
                } else {
                    new RegisterManager().execute(params);
                }

            }

        });

    }

    private boolean allInputsProvided(String[] params) {

        for(int i=0; i<params.length; i++){
            if(params[i] == null || params[i].equals("")){
                return false;
            }
        }
        return true;
    }

    private void setNationalitySpinner() {
        ArrayList<String> nations = new ArrayList<String>();
        isoCountryCodes = Locale.getISOCountries();
        Locale lang = new Locale("en");
        for (String countryCode : isoCountryCodes) {
            Locale locale = new Locale("", countryCode);
            String countryName = locale.getDisplayName(lang);
            nations.add(countryName);
        }

        Collections.sort(nations, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        nations.add("n/a");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nations);
        spinNation.setAdapter(adapter);
    }

    private void setGenderSpinner() {
        ArrayList<String> genders = new ArrayList<String>();
        genders.add("Female");
        genders.add("Male");
        genders.add("Other");
        genders.add("n/a");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genders);
        spinGender.setAdapter(adapter);
    }

    private void setYearSpinner() {
        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        years.add("n/a");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);

        Spinner spinYear = (Spinner) findViewById(R.id.yearSpinner);
        spinYear.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        hideAllIcons();
        return true;
    }


    private class RegisterManager extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String email = params[0];
            String name = params[1];
            String password = params[2];
            String yob = params[3];
            String countryCode = params[4];
            String gender = params[5];
            String localUserInfo="-1";


            String urlString = "http://" + MainActivity.server_url + ":" + MainActivity.server_port + "/register?email=" + email + "&password=" + password + "&name=" + name + "&yob=" + yob + "&country=" + countryCode + "&gender=" + gender;
            System.out.println("CALLING: " + urlString);

            URL url = null;
            try {
                url = new URL(urlString);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnection = null;
            try {
                System.out.println("get connection");
                urlConnection = (HttpURLConnection) url.openConnection();
                System.out.println("MongoDB URL called");
                String line = "";
                InputStreamReader isr = new InputStreamReader(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                localUserInfo = reader.readLine();

            } catch (IOException e) {
                System.out.println("Exception when registering");
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }

            System.out.println("DONE");
            return localUserInfo;
        }

        @Override
        protected void onPostExecute(String receivedUserInfo){

            if(!receivedUserInfo.equals(Constants.DUPLICATION)){
                MainActivity.user = new User(receivedUserInfo);
                finish();
            } else {
                Toast.makeText(RegisterActivity.this, "Email already in use", Toast.LENGTH_SHORT).show();
            }
        }


    }



}